import React from 'react';
import '../App.css';
import Header from './Header';
import Footer from './Footer';
import FormComponent from './FormComponent';

function App() {
  return (
    <div className="App">
      <Header />
      <FormComponent />
      <Footer />
    </div>
  );
}

export default App;
