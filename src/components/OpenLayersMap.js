import React from "react";

function OpenLayersMap(props) {
  const { loading } = props;
  const locationStyle = {
    display : "none"
}
  return (
    <div id="map" style={{height: "600px" }}>
      <div className="pointer" style={props.location ? null : locationStyle} id="overlay" title="overlay"/>
      <div className="location-popup" id="popup" title="Location">
        <div className="location-container">
          <div>
            Long:  
            { parseFloat(props.pointer_coordinates[0]).toFixed(2)},
            Lat: 
            { parseFloat(props.pointer_coordinates[1]).toFixed(2)}
            <br />
            {loading ? (
              <h5>Loading..</h5>
            ) : (
              <div>
                <img id="sunriseImage"src={require("../images/sunrise.png")} alt="sunrise" /> Sunrise: {props.sun.sunrise} UTC
                <br />
                <img id="sunsetImage" src={require("../images/sunset.png")} alt="sunset" /> Sunset: {props.sun.sunset} UTC <br />
                Daylength: {props.sun.day_length}
                <br />
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default OpenLayersMap;
