import React from "react";
import DatePicker from "react-datepicker";

function Form(props) {
  return (
    <div>
      <form onSubmit={props.handleSubmit}>
        <h2>Please insert latitude, longitude and date</h2>
        <div id="formcontainer">
          <label>
            <input
              type="text"
              value={props.latitude}
              name="latitude"
              placeholder="Latitude"
              required
              onChange={props.handleChange}
            />
          </label>
          <label>
       
            <input
              type="text"
              value={props.longitude}
              name="longitude"
              placeholder="Longitude"
              required
              onChange={props.handleChange}
            /> 
          </label>
          <label>
           
            <DatePicker
              selected={props.startDate}
              onChange={props.updateDate}
            />
          </label>
          <button>Submit</button>
        </div>
      </form>
    </div>
  );
}
export default Form;
