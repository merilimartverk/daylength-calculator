import React from 'react';
import Location from "./Location";
function Header() {
    return(
        <header>
            <h3>Daylength calculator</h3>
            <div id="userInfoContainer">
            <div id="time"></div>
                <Location />
            </div>
        </header>
    )
}
export default Header;