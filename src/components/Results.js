import React from "react";

function Results(props) {
  const { loading } = props;
  return (
    <div>
      {loading ? (
        <h3>Loading results</h3>
      ) : (
        <div>
          <h4 style={{color:"#3b3636", textShadow:"0.5px 0.5px grey"}}>Day length on {props.startDate.toDateString()}</h4>
          <h5 style={{color:"#3b3636", textShadow:"0.5px 0.5px grey"}}>Long: {props.longitude} Lat: {props.latitude}</h5> 
          <p> Sun rises at {props.sun.sunrise} UTC</p>
          <p> Sun sets at {props.sun.sunset} UTC</p>
          <p> Day length is {props.sun.day_length}</p>
        </div>
      )}
    </div>
  );
}
export default Results;
