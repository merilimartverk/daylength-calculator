import React from 'react';
function Footer() {
    return(
        <footer>
            <img class="footerImage" src={require("../images/sun1.jpg")} alt="sunset" /> 
            <img class="footerImage" src={require("../images/sun2.jpg")} alt="sunset" />
            <img class="footerImage" src={require("../images/sun3.jpg")} alt="sunset" />  
            <div id="footerText">
                <h5>CGI Home assignment</h5> 
                <p>2020</p>
            </div>
        </footer>
    )
}
export default Footer;