import React from "react";
import { usePosition } from "use-position";
const API = "https://api.sunrise-sunset.org/json?lat=";
const date = new Date();
const queryDate =
  "&date=" +
  date.getFullYear() +
  "-" +
  (date.getMonth() + 1) +
  "-" +
  date.getDate();
//gets user geolocation with usePosition hook
function Location() {
  const watch = true;
  const { latitude, longitude } = usePosition(watch);
  // gets daylength data according to user location
  function sunriseAndSunsetTimes() {
    if (latitude) {
      let apiurl = API + latitude + "&lng=" + longitude + queryDate;
      fetch(apiurl)
        .then((response) => response.json())
        .then((data) => {
          document.getElementById("sunrise").innerHTML =
            `<b>Sunrise:</b> ` + data.results.sunrise + ` UTC`;
          document.getElementById("sunset").innerHTML =
            `<b>Sunset</b>: ` + data.results.sunset + ` UTC`;
        });
    }
  }
  sunriseAndSunsetTimes();
  return (
    <div id="userInformation">
      <div>
        <b>Latitude:</b> {latitude}
        <br />
        <b>Longitude:</b> {longitude}
        <br />
        <div id="sunrise"></div>
        <div id="sunset"></div>
      </div>
    </div>
  );
}
export default Location;
