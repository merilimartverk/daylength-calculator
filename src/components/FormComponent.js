import React from "react";
import "react-datepicker/dist/react-datepicker.css";
import Results from "./Results";
import Form from "./Form";
import OpenLayersMap from "./OpenLayersMap";
import ReactDOM from "react-dom";
import { Map, View, Overlay } from "ol";
import { Tile as TileLayer } from "ol/layer";
import { OSM as OSMSource } from "ol/source";
import { MousePosition, defaults as DefaultControls } from "ol/control";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import GeoJSON from "ol/format/GeoJSON";
import Style from "ol/style/Style";
import Fill from "ol/style/Fill";
import GeoJSONTerminator from "@webgeodatavore/geojson.terminator";
import * as moment from 'moment';

const API = "https://api.sunrise-sunset.org/json?lat=";
const geoJSON = new GeoJSONTerminator();

class FormComponent extends React.Component {
  constructor() {
    super();
    this.state = {
      latitude: "",
      longitude: "",
      location: false,
      startDate: new Date(),
      date: new Date(),
      loading: false,
      showing: false,
      sun: [],
      center: [15, 23],
      zoom: 2,
      pointer_coordinates: [24.753574, 59.436962],
      newDate: new Date(),
      endDate: new Date(),
    };
    this.map = new Map({
      target: null,
      layers: [
        //base-map
        new TileLayer({
          source: new OSMSource(),
        }),
        // night&daytime layer
        new VectorLayer({
          source: new VectorSource({
            features: new GeoJSON().readFeatures(geoJSON, {
              featureProjection: "EPSG:4326",
            }),
          }),
          style: new Style({
            fill: new Fill({
              color: "rgb(0, 0, 0)",
            }),
            stroke: null,
          }),
          opacity: 0.2,
        })
      ],
      view: new View({
        center: this.state.center,
        zoom: this.state.zoom,
        projection: "EPSG:4326",
      }),
      //shows coordinates on hover
      controls: DefaultControls().extend([new MousePosition()]),
    });
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.showLocation = this.showLocation.bind(this);
    this.updateDate = this.updateDate.bind(this);
    this.updateEndDate = this.updateEndDate(this);
    this.updateNewDate = this.updateNewDate(this);
  }
 
  componentDidMount() {
    this.map.setTarget("map");
    this.map.on("moveend", () => {
      let center = this.map.getView().getCenter();
      let zoom = this.map.getView().getZoom();
      this.setState({ center, zoom });
    });
    this.popup = new Overlay({
      element: ReactDOM.findDOMNode(this).querySelector("#popup"),
    });
    //onclick popup with coordinates and daylength data
    this.map.on("click", (evt) => {
      this.popup.setPosition(evt.coordinate);
      this.setState({
        pointer_coordinates: evt.coordinate,
      });
      this.map.addOverlay(this.popup);
      const queryDate = `&date=${this.state.date.getFullYear()}-${
        this.state.date.getMonth() + 1
      }-${this.state.date.getDate()}`;
      const url =
        API +
        this.state.pointer_coordinates[1] +
        "&lng=" +
        this.state.pointer_coordinates[0] +
        queryDate;
      this.setState({ loading: true });
      console.log(url);
      fetch(url)
        .then((response) => response.json())
        .then((data) => {
          this.setState({
            sun: data.results,
            loading: false,
          });
        });
    });
    this.currentTime();
  }

  componentWillUnmount() {
    this.map.setTarget(null);
  }
//fetching daylength data from api: https://sunrise-sunset.org/api


  fetchData() {
    const queryDate = `&date=${this.state.startDate.getFullYear()}-${
      this.state.startDate.getMonth() + 1
    }-${this.state.startDate.getDate()}`;
    const API_URL =
      API + `${this.state.latitude}&lng=${this.state.longitude}${queryDate}`;
    this.setState({ loading: true });
    fetch(API_URL)
      .then((response) => response.json())
      .then((data) => {
        this.setState({
          sun: data.results,
          loading: false,
        });
      });
  }

  //if user checks daylength manually, then pointer will appear on the map
  showLocation() {
    console.log(this.location);
    let overlay = new Overlay({
      position: [this.state.longitude, this.state.latitude],
      element: ReactDOM.findDOMNode(this).querySelector("#overlay"),
      positioning: "center-center",
      stopEvent: false,
    });
    this.map.addOverlay(overlay);
    this.setState({ location: true });
  }

  //gets local time for the user
  currentTime() {
    let time = moment().format('h:mm:ss a');
    document.getElementById('time').innerHTML = `Local time: `+ time;
    
    setInterval(() => {
      time = moment().format('h:mm:ss a');
      document.getElementById('time').innerHTML = `<b>Local time:</b> `+time;
    }, 1000)
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value,
    });
  }
  updateDate = (date) => {
    this.setState({
      startDate: date,
    });
  };
  updateNewDate = (date) => {
    this.setState({
      newDate: date,
    });
  };
  updateEndDate = (date) => {
    this.setState({
      endDate: date,
    });
  };

  handleSubmit(event) {
    event.preventDefault();
    this.fetchData();
    this.setState({
      showing: true,
    });
    this.showLocation();
  }
  render() {
    const { showing } = this.state;
    return (
      <main className="form-component">
        <Form
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
          updateDate={this.updateDate}
          {...this.state}
        />
        <br />
        {showing && <Results {...this.state} />}
        <OpenLayersMap {...this.state} handleSubmit={this.handleSubmit} />
      </main>
    );
  }
}
export default FormComponent;
