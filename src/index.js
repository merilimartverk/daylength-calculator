import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './libs/v6.3.1-dist/ol.css'
import App from './components/App';


ReactDOM.render(
    <App />,
  document.getElementById('root')
);


